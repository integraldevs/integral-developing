<a name="top"></a>
<div align="center">
<img width="150px" src="https://commondb.net/android-chrome-192x192.png" align="center" title="Common DB">

# Common Database

aka:

**Data of the Commons (DotC)**   
**Common Distributed Data Network (CDDN)**   
**Distributed Open Collaborative Platform (DOCP)**

&nbsp;

*Draft Paper Version: **v2.3**   
February 10th, 2020*

Sebastià Freixa Povo - Valentina Messeri  
[commondb.net](https:/commondb.net)

</div>

&nbsp;

&nbsp;

### Taglines:


- *Would you like to engage in a conscious data sharing and participate in building the common good? Join the CommonDB!*

- *A missing data sharing structure to improve the common good worldwide.*

- *A huge common database to store all words and knowledge on a resilient, fast, scalable, open-source and distributed p2p system.*

- *Be the owner of your data, and decide who has access to what about you. Stop feeding the business of data corporations and companies selling what they know about you.*

- *An optimal source of types of stuff, verbs-actions, agents public data, location names, units and other kinds of data for any common good platform or software.*

- *A network to store and share any type of information or data with a quality auto-regulation system performed by all the peers, avoiding external censorships.*

- *An extensible modular multi-tool to search for or contribute to common useful information, to join or create teams and projects, to communicate with people, to donate, exchange, buy, sell or rent resources of any kind, and account them as a real common [REA](/rea-chain.md) stream of economic events.*

- *The rise of artificial common intelligence distributed platform, built by the people for the people.*

&nbsp;

&nbsp;

&nbsp;



## Index:<a name="index"></a>

- [Introduction](#intro)
- [What?](#what)
- [Why?](#why)
- [How?](#how)
  - [Governance](#governance)
  - [Technically](#technically)
- [Layers:](#layers)
  - [Identity layers](#identity)
  - [Reputation layers](#reputa)
  - [Encryption layers](#encrypt)
- [Backend](#backend)
- [Frontend](#frontend)
  - [Mockups](#mockups)
- [Nodes](#nodes)
  - [Agents](#agents)
  - [Membranes](#membranes)
  - [Node parts](#node-parts)
- [Data Atoms](#data-atoms)
  - [Data spaces](#data-spaces)
- [Human Teams](#human-teams)
- [Milestones](#milestones)

<div class="top" align="right">

[^ top](#top)

</div>

&nbsp;


## Intro:<a name="intro"></a>

&nbsp;

See the presentation [slides](https://commondb.net/slides.html)

The IntegralDevs Team, whose goals are explained [here](https://gitlab.com/integraldevs/integral-developing), is developing a multi-device App with a simple frontend to browse, build and manage a resilient p2p scalable blend of distributed databases to store all Common Data to share between platforms and between agents in a conscious and informed way.

This App is built modular, very simple in its basic version, but extensible with plug-ins added within groups/projects you might join, so that tools and options can enhance the features of the basic app, depending on personal interests and data stored in each profile.

This software would like to became a improved version, distributed, faster and highly scalable of the Open Collaborative Platform ([OCP](https://wiki.p2pfoundation.net/Open_Collaborative_Platform)), a software already in use as common db backbone of cooperative services such as [FreedomCoop](http://freedomcoop.eu/) and [Bank of the Commons](https://bankofthecommons.coop/), though refactored with the most advanced open-source technologies in the NoSQL databases field and the fastest programming languages to allow the infinite growth of the data size, and being capable of delivering fast queries to ensure high availability.

The first basic core function of the App is to provide a secure and private digital identity to agents/users who log in to participate: this is a multilevel process, starting with the confirmation of the email address or phone number and creating a cryptographic digital signature (like PGP), however other identification resources could be added to implement certain features; e.g. to create a complete health profile a physical/biological sample could be provided to define the health status of the agent.

Once properly identified the agent is enabled to build a profile including public and private parts, and then use the App frontend to browse the network where other Agents are active and where a range of actions can be taken, for example:

* contribute to the network simply by sharing valuable and useful data, knowledge of all sorts, resources of any kind, or program code
* create, search/find and join groups/projects,
* buy products, services or project's shares,
* receive or donate funds in a crowdfunding campaign,
* build and map your project online to offer your products or services,
* communicate with other agents or projects/groups,
* share your files with friends or with everyone,


In the Commondb network space, all agents can create different projects and each of them offers Tools to their members. Tools that act as features of the basic App, integrated as plug-ins, ranging from currencies accounts, tasks management, tracking of resources and workflows, processes and internal project plans, up to accounting tools, billing or contracting in full legality, and also governance tools suitable for debates, polls and voting system, etc..

In the context of this exponential growth, it is worth highlighting how these tools are built from public data atoms (common data), some of them containing code that is executed by the nodes (devices and supernodes); this way any tool can be updated by just updating the local replica of the shared data atoms in the distributed common database.

Due to the ongoing collaboration, this App would be the optimal future for the already mentioned OCP, and also for
[CoopFunding](https://coopfunding.net/) and FairCoop ecosystem tools such as [UseFaircoin](https://use.fair-coin.org),
[GetFaircoin](https://getfaircoin.net/) and [FairMarket](https://market.fair.coop/), to start with, but also for many others to serve the Common Good in general.

In this regard, it is interesting to remark how, with the involvement of the project's working groups, but also of all agents/users and the infinite modular possibilities of the tools modules, the CommonDB has the potential to become a resilient and multi-version Wikipedia (or Wikidata) of global knowledge, a better open-source alternative to Telegram or WhatsApp for communication, a common p2p file-sharing system, a private but resilient vault for personal health data, a common AI and knowledge source which is neither privatizable by design nor convertible into a capitalist profit instrument.

<div align="right">

[^ index](#index)

</div>

&nbsp;

## What?<a name="what"></a>

&nbsp;

Each app instance is a node of various types of p2p databases; a polyglot system integrated as a single and easy to use interface.

Each agent/node chooses which data to store locally, depending on the physical device used for that purpose (drive space/memory), the bandwidth and ultimately its own interests.

Besides, the App itself suggests which data types require multiple nodes to be fast and resilient enough to meet the demand for those data within the entire network.

The mesh of nodes created can store both personal and private data owned by individuals or groups/projects with various layers of encryption (see [below](#encrypt)) with the benefit of improving several aspects of people's lives by adding to the basic app, plugins to manage personal or collective data regarding economic facts, health, education, communication, transport, manufacture, etc.

<div align="right">

[^ index](#index)

</div>

&nbsp;

## Why? <a name="why"></a>

&nbsp;


In a global big data scenario, we regard it is necessary to develop a resilient structure to foster data sharing with a view to raise human consciousness, increase awareness, knowledge and the ability to love and care; to avoid political censorship and also avoid feeding the business of capitalist corporations free of charge.
And also to address the issue of repeating data in separate databases, as well as the disconnection of data silos when their APIs do not effectively communicate with each other when updating data from different sources.

With the aim of consolidating the practice of Common Good at a global level, we have the possibility to create a CommonDB made of common data, such as alphabets and words of each language. This semantics will be further expanded with all verbs or terms that define arts and depict facts/actions and relations; with nouns given to parts of nature, to human beings, to places, inventions, units, types of things, concepts...

There is an infinity of data available to share and make common if we wish to improve as human beings.

The current situation shows a system of API's updating and replicating data between different platforms, which has its own usefulness and in our proposal, there is room for common vocabularies and protocols (like [ValueFlows](https://valueflo.ws/), [ActivityPub](https://www.w3.org/TR/activitypub/), [RDF](https://www.w3.org/RDF/), etc.).

The development outlined here focuses instead on building a distributed database capable of becoming the main source of common data shared between different platforms. Indeed, words and protocols used in the API mentioned above will be implemented to import or migrate data from or to this common database.

<div align="right">

[^ index](#index)

</div>

&nbsp;

## How?<a name="how"></a>

&nbsp;

### Governance: <a name="governance"></a>

Yet, to achieve the objectives we set ourselves within the Common Good framework, e.g. to facilitate the backbone software for ethical projects and attain the several goals avoiding in the meantime misuse and abuse of the system, some important conditions must be established.

A myriad of participation tools are necessary, and thanks to its encryption and authentication system the Commondb is a candidate to fulfil the purpose of a voting tool or a distributed decision-making system.

It is equally necessary that the good use is promoted as well as the bad use discouraged starting from the program code itself.
This is to say that the policy of recovery and improvement of ethical values must be written in the code itself so that the tool could be fair by its very nature, by means of participation mechanisms, transparency, open licences, etc.

In this way, it is possible to promote and improve Quality globally, thanks to self-regulated control by the same openness and transparency of the software, adding ethics values intrinsic written in its code.

This data network is designed to become one day self-governed (like the www), based on the assumption that there are more people of goodwill than of bad intentions.

The nodes themselves choose the data they are interested to host, use and support, so the malicious or fake data should have less network support and fewer data resiliency. Consequently useful and healthy data became popular and resilient.

Each node/agent, even if in possession of economic resources, space, speed, bandwidth, etc., will have no more voice than other less "powerful" nodes: all nodes have the same rights and political power in the network of "one voice" regardless of their capabilities.

More economic possibilities of any other kind should not be a way to have more visibility or power, only recognition and people's support, based on quality parameters, give popularity and resilience to the data.

On the topic of self-governance, the principle of intrinsic Ethics is applied, therefore a series of rules have been thought a priori from the very inception of the project, and are articulated in the following way:
during the development of the core tool and the community around it, i.e. before the network is ready to be self-governed, a human team with healthy goals and resilient internal harmony is essential to protect Values and common good goals.

Protection is necessary against unhealthy influences coming both from people not aligned with our principles and from capitalist corporate pressures willing to bias the project to their interests or willing to prevent the development of this common good meta-tool and consequently keep the status-quo.

The IntegralDevs team, whose goals are detailed [here](https://gitlab.com/integraldevs/integral-developing), can try to hold that role of keeping the initial values on the political key development decisions, defending the healthy common good goals originally stated from the real core of the tool, the developers’ side,  who maintain sound ethical principles and consider cooperation and collaboration value in itself.

<div align="right">

[^ index](#index)

</div>


### Technically:<a name="technically"></a>
Whether some blockchain technologies could be optimal to store non-mutable data like economic transactions, they are not good for an agile management of other types of data (mutable-data), for that purpose we reckon is better a mix of distributed NoSQL databases, like a graph db for relations, a key-value store db for words, types and short texts, a key-doc store for long texts or code, and a p2p file-sharing system for files.

The proposed App, in its first version, should allow using the best db technology for the type of data being saved:
- some of the economic events can be stored in a flexible blockchain (like [FairChains](https://www.fairkom.eu/en/fairchains), [Holochain](https://holochain.org) or [Chromia](https://chromia.com)) used as a "[REA chain](/rea-chain.md)", once there is consensus between the involved parts in an exchange.
- other economic events like transactions of the many cryptocurrencies around (starting by FairCoin) are already stored by their blockchains
- the words of the languages and their translations (if exist), the types of resources, types of skills or arts, the names of locations and areas, the units and the unit ratios, and many other types of data can be better stored in a key-value distributed JSON datastore, like the core Ddb of the app.
- the images, the music, the videos and all heavy data is better stored and shared like in a p2p common filesystem (or torrent style).
- the long texts, the code libs, templates, scripts, etc, can be stored better in a key-doc Ddb


To achieve this we need to join forces in harmonic dev teams and some key ingredients:
- a super scalable multi DB system that can grow indefinitely by adding people’s nodes ([in a p2p style](http://wiki.p2pfoundation.net/P2P_Networks)),
- an integral data model that makes easy to manage the growth of the types of data and the relation between them ([proposed here](/integral-data.md)), with...
- a set of widely consensus Common Resource Type Trees [\*](#note1), where representative agents of the different sectors of activity take care of organizing and curate the needed and useful resource type branches and sub-types to use in that sector (alimentation, communication, etc.) which is an organic, always mutable process and this data partitioning in sectors and keywords allows us to develop...
- an engine to analyse the network, update distributed indexes and balance the load by asking for more nodes helping in critical data sectors and all this back-end needs to be built with...
- a fast enough functional language like Erlang-Elixir, JavaScript, but also Rust and others, and all this needs to be easily managed from...
- an easy-to-use Graphical User Interface, that simplifies the ‘brow-serving’ and the configuration options. And that allows...
- a myriad of modules to add functionalities, pages and options to the basic App, to do a lot of things, using the best open-source code available worldwide.


The data you browse or use from the Ddb, cached in your device, is automatically served to other nodes by default, except private data or private browsing data, which defaults to no sharing but the agent can choose trusted peers to spread his encrypted private data only to their devices.

<a name="note1"></a>(\*) The most of the Types of things can be common (and perhaps some particular subtypes used by a single project
don’t need to be common).

<div align="right">

[^ index](#index)

</div>

&nbsp;

## Layers:<a name="layers"></a>

&nbsp;

### Identity layers:<a name="identity"></a>

The identification of a real agent by the App, is related to the amount of information that is possible to verify the agent profile. That behaves like 'KYC' tiers or Levels of a "trusted operator" acting as layers of visibility and functionality in the UX. Some agents can enter directly in the second layer, avoiding the first (if they don't use email). Every tool will set-up its behaviour related to this identity layers of the agents (depending on their goals):
- First Level:
  - anti-robot strategies must pass
  - an Email must be confirmed (by a sent code)
  - this email must be Unique in the worldwide data network
- Second Level:
  - a Phone number must be confirmed (by a sent code)
  - this Phone must be Unique in the worldwide data network
  - anti-robot strategies must pass (if never checked)
- Third Level:
  - both Email AND Phone are confirmed OR enough other data validates the user as a real human being
  - the agent must have enough relations already set with other peers, and that peer are known active nodes to the system
  - the related other agents should agree on the mutually trusted relationship, and some data might have to be exchanged
  - there are no ongoing claims about duplication of identity details
- Fourth Level:
  - a proper Membership process has been fulfilled with any of the network trusted projects (with or without shares or payments, depending on each project's politics)
  - some data about the agent is public and can be verified by peer-review
  - the agent has a distinct profile Image (avatar)
  - if required by a tool:
    - location data can be verified with the phone GPS (if allowed)
    - a legal ID card or passport scan, or other documents can be required (and uploaded)
- Fifth Level (with a physical step):
  - some entities will set-up a required physical step in their membership to verify agents in-person (e.g. for legal voting, but also for candidates/applicants interviews, health diagnostics, etc)
  - some biomedical projects will require a physical step to also get biologic samples of the agent (e.g. blood, urine, DNA, etc.)

As identity levels, this doesn't directly reflect Reputation but is the main factor of it. All means of reputation scoring are based on the agents' identity as single profiles, so a more verifiable identity (trusted as a real human) will give force to the reputation ethical algorithms.

In the case of collective agents, they can arrive at Fourth Level about identity, but some of their individual coordinator agents should be known also, at least at Third Layer.

Other means of auto-discovering lies and fake information in the profiles, relations and actions, and their changes/edits in history, will be used to track the validity of the agent as a trusted human and its just started Reputation process.

<div align="right">

[^ index](#index)

</div>

### Reputation layers:<a name="reputa"></a>

The system will provide means to track different ethical values (Quality), related to the agent's actions, economical facts, communication, tasks, etc.
The atomicity of the semantic data model in combination with the sharing of a lot of common data atoms in a common stream of economic facts (REA), give us the opportunity to build on top of this network a proper set of ethical algorithms that can automatically account for values like:
- Organic ingredients (GMO-free, Raw, Vegan, etc.),
- Eco-sustainable (energy, waste, toxicity, etc.),
- Fair-trade (non-exploitation, shared revenues, etc.),
- Non-violent Communication (by semantic analysis of public texts) and Behaviour (no claims),
- Contributions to the Common Good (by donations, tasks, open-source, open-hardware, open-knowledge, art, etc.)
- Usefulness and quality of resources or data atoms produced,
- and in general the respect of the Universal Human Rights.

That algorithm will produce a public means to determine the reputation (trust, honour, karma/dharma) of any agent, as an overall average or related various single particular metrics. The system can show a simple automatic Quality "stamp" on projects and agents (or in deep reports), both for good or bad qualities.

Each of the particular values accounted, or a combination of them can act as a layering feature of information in the GUI (if the agent wants to filter-out "bad stuff").

The development of said Ethical Algorithms to build reputation metrics is a huge part of the whole CDB project, open to the participation of skilled, clever and healthy people.
These ethical metrics are planned for the second or third main release of the software, once many core tools are ready, in place and properly running. A minimum first version of this ethical metrics should be in place before going live with real data in production.

<div align="right">

[^ index](#index)

</div>

### Encryption layers:<a name="encrypt"></a>

The encryption of the agent’s data has different levels, like layers, depending on the relation between the browsing agent and the data creator agent. The app should offer from easy sets of privacy config options up to a more granular configuration of the privacy settings. For example, the personal information about Health can be shared only with family, friends or a few chosen organizations nodes, but in case of emergency, any accredited doctor can gain access to it.

Some layers of encryption for the parts of the agent’s data:
- a first layer to hide info to non-logged users
- a second layer that only friends of the agent can access through (can be n levels),
- a layer of technical data that only the app itself manages,
- a set of layers for strictly personal data: can be defined as a special encryption layer for medical data with different rules than a layer with profile data, education data, economic data, etc.

<div align="right">

[^ index](#index)

</div>

&nbsp;

## Backend:<a name="backend"></a>

This meta-projects based on a concept of full flexibility and modularity totally Agent-centred, therefore what we call 'the back-end' is basically the database because nearly all the logic and code is also inserted as db records. To render a page the system gathers some objects from the db, not only the requested information but also the HTML template, js, css or any code snippet or lib needed to show the data. This way, to improve some code or fix a bug, it's only needed to edit a db record and all uses of it will change its behaviour globally. So, the code required to run any logic is gathered from the db, and then the only function of the back-end layer is the databases management and the interpretation of the code coming from the ddb.

The network will have **nodes** and **super-nodes**, being the super-nodes just like regular nodes but offering 24/7 service and dedicated space, memory and bandwidth (so using the actual servers around as helpers of the network). The code for the super-nodes can be initially different from the one for regular nodes (an extension of it), but the goal is to put all logic of extensions as db records, and a regular node can become a super-node just upgrading its local app with the functionalities available if the node joins the super-nodes group in the network (a moderated process).

The main parts of the backend code are:
- **connectors** from the distributed databases used, depending on the type of data, to the frontend GUI.
- **encryption mechanisms** to ensure:
  - privacy of personal data in various layers, and
  - to guarantee a private login authentification in the system.
- **interpreters of the code** received as db query results: at least the basic languages used to run the basic functionalities of the app should be interpreted at the core level (rust, erlang, python, etc)
- **engine to regulate the load balance** between nodes, improve resilience and availability of the data (depending on its popularity):
  - should warn nodes (in the frontend) about required help hosting crucial popular data and
  - let them choose sectors of data to host from their interests.
- **unified indexing engine** joining the indexes of every used database system together, to distribute a self-building index of all the stuff in the ddb's to deliver:
  - fast searches of text data (full-text search) and
  - faster retrieval of precisely referenced items (by key or uid)
- **distributed caching system** multi-level, splitting the cache into levels:
  - from most common resources (highly available) to more specialized resources (less distributed)


The logic of this engines can be recorded as db 'rows' (and run them in the node once retrieved from the db) to update them globally when any bug fix or improvement is achieved after launch. If this is too hard to achieve from start, we can start with that engines being part of the hardcoded initial code and in another release make that engines easily updatable as db records.

The goal is to make the basis generic enough to avoid needing updates of the app and rely as much as possible on
'**code from the db**' techniques (like did the [Maid-SAFE network](https://maidsafe.net)).

See the [Node Parts](#nodeparts) diagram for a visual draft of the main parts.

<div align="right">

[^ index](#index)

</div>

## Frontend:<a name="frontend"></a>

The GUI can be developed mainly as a multiplatform app, for mobiles and computers. We can develop also a web version if we find good ways to identify users safely enough and trust at least some browsers.

The main parts of the frontend are:

- **authentication** of the user agent: the proposal is a GPG alike auth system, and are needed:
  - ways to recover access if an agent loses her/his credentials, with a progressive KYC process, at the pace of the user, to have alternative access recovery ways (alternate emails, phone number, address, etc).
  - also requires ways to cancel active accesses to personal data if the access is stolen or compromised in some way
- **profile building and privacy settings**: the UI to add data of the agent (individual or collective) and:
  - define what visibility and sharing settings affect each part of the profile data.
  - if there are not yet trusted peers to spread the profile data, the UI should allow setting some, starting to define the first social relations for the db system to work distributed and have your data there even if you are not online.
- **human relations and communication**: the user relations are the main source of permissions and resilience of the user's data for the whole system
  - joining process of the user agent with projects of her/his interest and communication means with the coordinators of such projects
  - communication means for any group, both for internal private conversations and for open communications seen by everyone
  - private personal communication with other peers, offering options to delete, expire or edit messages
- **economic facts**: this part of the UI has some sub-parts:
  - a generic system to represent **'exchanges'**: donations and received gifts, buys and sells, acquired shares, payments and bills, etc.
    - the currency wallets or accounts of the agent's coins or shares (it can manage internal real wallets or just show defined accounts activity by pulling-pushing data from-to the proper source)
    - the UIs to use the active payment gateways of the projects you interact with
    - a multi totals and resources system where the agent can track and make accountings of any type of resource or stock
  - a generic system to represent **'tasks'**:
    - from personal todos to assigned tasks when participating in projects processes or workflows,
    - remunerated or exchangeable work (normally related an exchange),
    - search for tasks related your skills and search people with certain skills (if they stated that info as publically is known)
  - this two economic GUIs are mostly read-only: to create new economic facts is needed special app-tools that are available or not depending on the user relations, the main source of permissions. For example:
    - if the user agent is an active participant in a project like FairMarket or similar, then the UI tools to create a shop and define products becomes available.
    - if the user agent wants to join a cooperative, buying shares, the payment gateways options defined by the project became usable by the user (and manageable by the project's coordinators).
    - if the user demonstrates ownership of certain accounts (e.g. cryptocurrencies), the account managing options should appear in the economic UI (or links to the account manager tool it is fiat or a not integrated yet currency)
    - if the user participates in projects with activated tasks and workflows, the needed tools to define the related tasks commitments and events appear available for regular users (or to define plans and process setups if the user is a coordinator of the project)
- **brow-server**: general search discovery of knowledge and network resources
  - the searches work using agent's relations too, searching first in known sources and after sending queries to any part of the network, but not flooding all nodes with search requests.
    - The knowledge already present in the system (one day all words) and a common set of data 'sectors' or common resource types trees [\*](#note1), allows us to send queries for index data or real data only to the nodes known to be hosting that type of keyword data.
  - navigation of web pages stored in the db
  - sharing settings, to define how to share the cached browser data from the navigation
  - the UI should allow contributing to the network by adding knowledge, words and definitions, like in a Wikipedia style but decentralised
- navigation of normal external web pages from the internet (served by servers) can be added if needed (may be useful to crawl data into the common db) but there are plenty of existent browsers for this.


Many more functionalities can be added through adding plugins to the app, available (or auto-installed) depending on the relations of the agent with different projects and her/his interests or working fields.

See the [Node Parts](#nodeparts) diagram for a visual draft of the main parts.

<div align="right">

[^ index](#index)

</div>


### Mockups:<a name="mockup"></a>

Some mockups to define a first **GUI for small screens**. See them as slides in the presentation [Slider](https://commondb.net/slides.html) on the penultimate page (use vertical arrows to move). 
You can also click on the link below every image to see it full size.

| | | |
|:-:|:-:|:-:|
| ![](https://commondb.net/images/android1_x3.png) | ![](https://commondb.net/images/android2_x3.png) | ![](https://commondb.net/images/android3_x3.png) |
| *__Mock.1__: Login ([full size](https://commondb.net/images/android1_x3.png))* | *__Mock.2__: Register Start ([full size](https://commondb.net/images/android2_x3.png))* | *__Mock.3__: Confirm email/phone ([full size](https://commondb.net/images/android3_x3.png))* |
| ![](https://commondb.net/images/android4_x3.png) | ![](https://commondb.net/images/android5_x3.png) | ![](https://commondb.net/images/android6_x3.png) |
| *__Mock.4__: Register End ([full size](https://commondb.net/images/android4_x3.png))* | *__Mock.5__: Upload Image ([full size](https://commondb.net/images/android5_x3.png))* | *__Mock.6__: Dashboard, Toolbox, Profile ([full](https://commondb.net/images/android6_x3.png))* |
| ![](https://commondb.net/images/android7_x3.png) | ![](https://commondb.net/images/android8_x3.png) | ![](https://commondb.net/images/android9_x3.png) |
| *__Mock.7__: Scan Contacts ([full size](https://commondb.net/images/android7_x3.png))* | *__Mock.8__: Search Projects ([full size](https://commondb.net/images/android8_x3.png))* | *__Mock.9__: Create Trust Groups ([full size](https://commondb.net/images/android9_x3.png))* |
| ![](https://commondb.net/images/android10_x3.png) | ![](https://commondb.net/images/android11_x3.png) | 
| *__Mock.10__: Group Peers, Shared Data ([full](https://commondb.net/images/android10_x3.png))* | *__Mock.11__: Settings ([full size](https://commondb.net/images/android11_x3.png))* |

&nbsp;


<div align="right">

[^ index](#index)

</div>

&nbsp;

## Nodes:<a name="nodes"></a>

&nbsp;

**Nodes** are instances of this software in one device (big or small), and when a device is big enough can act as a super-node (helping the network by design).

The App scales up or down in functionalities depending on the ddb records it holds, some of them contain code that is executed in that node, and as such, they behave like plug-ins that can be easily activated or deactivated.

&nbsp;

### Agents:<a name="agents"></a>
 
All nodes or devices of a single person (say, phone and laptop) will keep in sync the same used data, a single **Agent** profile, but the bigger device will hold the history of versions and any big file you'll not store in a phone.

Agents are the entities in society and Nature. Agents can be **individual or collective**, but a collective profile to be managed must be composed of individual members with their profiles, and some of them acting as coordinators of that collective identity (say, an assembly, a company, etc.). This is to identify the economic agents (and their components if is a collective) in a common REA stream of economic events.

In such a stream each Agent will be uniquely identified by a public-key, related a readable worldwide unique 'username', acting as a UID. Also, each Node on each device will have another UID, transparent and not even known to the user.

<div align="right">

[^ index](#index)

</div>


### Membranes:

Each node is like a natural cell, with a **Membrane** that does some main functions:

*  Show its Device UID to the network (the peer node id)
*  Show the Agent UID or other public data to known peers
*  Allow connections to and from known trusted nodes
*  Block connections to and from untrusted nodes
*  Validate and clean outgoing queries to the network for searched data


Each person relations will determine its network of trusted and untrusted peers, and the data will move accordingly to the user's nodes Membrane Settings.
<div align="right">

[^ index](#index)

</div>


### Node Parts:<a name="nodeparts"></a>

All nodes will have the main parts represented in the diagram:


| |
|:-:|
| <img width="850px" src="https://commondb.net/images/CDB_node-parts.png"/> |
| *__Fig.1__: Node Parts ([full size](https://commondb.net/images/CDB_node-parts.png))* |

<div align="right">

[^ index](#index)

</div>


## Data Atoms:<a name="atoms"></a>

&nbsp;

Data Atoms are small single bits of information. They can be single words or numbers, but also can represent images and other files. When a data atom references various other atoms they can be named Molecules: lists, JSON objects, functions and variables. We'll use the term 'Atom' for both cases in these texts and diagrams.

Normal texts are lists of words. More complex structures like trees can be defined with JSON objects. Functions normally accept arguments (atoms or molecules) and reference other atoms before returning a value. 

Sub-atomic particles are the unicode characters and symbols that form the words and the code. They will be in all nodes.

These Nodes all share a lot of **Common Data Atoms**, such as the words of the various languages, the names of things and places, etc. but also code libs and functions, templates, etc. These atoms are public, some of them pre-installed, some will download at startup and some will download on demand.

Each Node also produces **Personal Data Atoms**, such as identity or contact details, economic transactions, health history, communication with others, etc. Some of these personal data atoms are public, some are private, and some are in between those extremes: shared only with some peer-nodes arranged in custom trusted groups (defined by every Agent).

Personal data **Atoms will have two signatures**, the one of the human Agent and one of the Devices (the App node). So each atom has an authorship 'shape' (represented as different single geometric shapes in the diagrams below). The agent signature is always needed to decrypt private data, but the device signature is just very useful for security and data or access recovery workflows.

&nbsp;

### Data Spaces:<a name="spaces"></a>

In each Node there's three flexible basic **Data Spaces** (storage warehouses):

*  Public Space (space to store public atoms, authored by anyone)
*  Private Space (with access, to store personal data and know secrets of trusted peers)
*  Secret Space (without access, to store secrets of friends)


Each node decides how big or small are these data spaces, depending on capabilities and other factors. See [Seq1](https://commondb.net/images/CDB-nodes-atoms-1.png) diagram.

When two trusted nodes connect, they share the public atoms that they can need from each other, storing them in their public data space. Depending on the level of trust between each other, and the attitude of their membrane settings, they can also share private data in two ways:

* with read access, stored in the private space, like a shared secret.
* without read access, stored in the secret space, a vault of unknown secrets from friends.


See diagrams [Seq2](https://commondb.net/images/CDB-nodes-atoms-2.png) and [Seq3](https://commondb.net/images/CDB-nodes-atoms-3.png) to visualize data atoms movements between nodes and their membranes.

Super-nodes in trusted hands are good helpers if they allow a big space for Secret data, so people can store replicas of their atoms there (to increase resilience) without giving any access to the information stored.
See [Seq4](https://commondb.net/images/CDB-nodes-atoms-4.png) diagram.  
&nbsp;

| | |
|:-:|:-:|
| <img width="410px" src="https://commondb.net/images/CDB-nodes-atoms-1.png" title="Seq 1: Node's Data Spaces and Atoms"> | <img width="410px" src="https://commondb.net/images/CDB-nodes-atoms-2.png" align="right" title="Seq 2: Data Atoms in Nodes"> |
| *__Seq.1__: Node's Data Spaces and Atoms ([full size](https://commondb.net/images/CDB-nodes-atoms-1.png))* | *__Seq.2__: Data Atoms in Nodes ([full size](https://commondb.net/images/CDB-nodes-atoms-2.png))* |

&nbsp;

| | |
|:-:|:-:|
| <img width="410px" src="https://commondb.net/images/CDB-nodes-atoms-3.png" title="Seq 3: Data Atoms in Nodes"> | <img width="410px" src="https://commondb.net/images/CDB-nodes-atoms-4.png" align="right" title="Seq 4: Data Atoms in Nodes and Supernodes"> |
| *__Seq.3__: Data Atoms in more Nodes ([full size](https://commondb.net/images/CDB-nodes-atoms-3.png))* | *__Seq.4__: Data Atoms in Nodes and Supernodes ([full size](https://commondb.net/images/CDB-nodes-atoms-4.png))* |

&nbsp;

<div align="right">

[^ index](#index)

</div>

&nbsp;

## Human Teams:<a name="human"></a>

&nbsp;

This project requires at least two initial sub-teams, to start in parallel but under the same philosophy of the IntegralDevs main project:

- The **code developers** team and
- the **communication-coordination** team (or facilitators).


The **coordination team** should help from start to:
- communicate the project worldwide
- interface between the core devs and the software users or potential users
- manage a crowdfunding campaign in phases
- engage patrons, donors and partners of the common good scene
- find and try to involve healthy and skilled people for building the first needed Teams:
  - **Security team** (testing and improving privacy, stability, etc)
  - **Back-end team** (improving data CRUD modules for the various ddb's used)
  - **Front-end team** (improving usability and graphic design as modular skins)
  - **Communication team** (improving the project's social connections, translations, etc)
  - **Common Taxonomy team and sub-teams** (improving-curating the basic main branches needed but mainly improving coordination of 'sectors of activity' in sub-teams to build consensus about common types or data useful in their sectors).


<div align="right">

[^ index](#index)

</div>
&nbsp;

## Milestones:<a name="milestones"></a>

&nbsp;

### Milestone #1: MVEP<a name="Milestone #1"></a>
The first big milestone in the roadmap will be a first usable beta version of the minimum layer functionalities to add app plugins or components to start, at least:

- authenticating users and allow the building of simple agent profiles with location data, so...
- mapping agents that wanted to share their location, organised in map layers to reflect types of agents, etc
- filling up the db with useful common data and words (verbs, types, agents, units, unit-ratios, location and region names, definitions of concepts, etc)
- joining projects (with moderated access or autojoin link),
- exchanging resources (money to start with) for project's shares when they are needed for membership, that means...
  - having active payment gateways (defined by the projects),
  - tracking of exchange transfers, commitments and events,
  - showing transfers status and allowing to edit payment status if the user is the coordinator of the project and the gateway is manual,
  - some payment gateways can be fully automated, like cryptocurrencies and, up to some degree, also the credit-cards.
- communicate with the project coordinators (or candidates and members if the user is a coordinator), at least via chat
- settings of the users' data privacy, visibility and sharing scopes
- settings to allow your node to host data of your interests, of your friends and keen organisations, etc (depending on the used device capabilities)


This first version can allow then the migration of:

- the whole actual OCP membership system, with the custom forms, custom payment gateways of custom shares, etc
- the UseFaircoin and other simple mapping solutions for projects, not only in the common good ecosystem, but also mapping company providers, institutions, or any type of organization (in map layers).


Technically this first beta needs the node's backend functionality to:

- authenticate the user cryptographically
- connect and use shared data from the main core JSON distributed DB (for light mutable objects data, like profiles, words, etc)
- connect and use shared data from the GraphDB or other 'relations' storage system (agent-to-agent, agent-to-resource, verb-to-verb, verb-to-name, etc)
- connect and use the shared files system, to retrieve at least various common code libs and to save also the agent's logo or avatar images in a distributed way
- decrypt query responses with the required keys depending on the user agent identity and relations (permissions) and the levels of privacy applied to the data when recorded
- restrict the disk space, memory and bandwidth usage depending on the used device capabilities
- load-balance of partitioned data amongst peers by type and popularity: very used data types should be in many nodes to improve availability and db response speed
- send notifications to users (at least via email)

&nbsp;

### Milestone #2:<a name="Milestone #2"></a>
In the second iteration or core code release, we can include (once the first beta is tested and fixed enough to become 'stable') app plugins to:

- allow projects to define other assets 'for sale' apart from membership shares, like cryptocurrencies directly or products of any kind
- allow projects to define custom crowdfunding campaigns, with custom payment gateways, self-management tools, etc
- communicate with other single users in a safe encrypted way using the app
- communicate in group's private and public chats, (allowing the organisation and debates of every sectorial group, broad or small, for them to have consensual common sets of resource types, skill types, units and other stuff used in that sector of activity).


This second milestone can allow then the migration of:

- the GetFaircoin website functionality and other simple exchanges solutions that mainly sell cryptocurrencies
- the CoopFunding website functionality
- the whole FairMarket and other online multi-shop systems can be ported to this platform
- the communication tools like Signal, Telegram or Whatsapp can start to be less used to coordinate people
- the behaviours of the social network can be imitated and ported in this opensource system to stop using corporate solutions that make business with our data


&nbsp;
### Milestone #3:<a name="Milestone #3"></a>
In a third iteration or release, we can include plugins to:

- allow agents to store their health data facts and history, individually and via health facilitators and health organisations, with special access rules adapted to health data.
- allow projects to create survey polls and allow agents to vote them or submit opinions, and other tools to improve governance of any common good project
- allow projects to define plans and tasks of productive workflows or transformation steps in manufacture, input resources and outcome products or services, reusable process recipes, etc
- ...


<div align="right">

[^ index](#index)

</div>
