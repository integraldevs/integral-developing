# The Arts of Living #
Developing a constructive attitude for a better world
v0.1.2
&nbsp;
## What is Art? ## 

In a healthy and nature-connected human society, we'd like <b>all Verbs in any language to be considered as Arts</b> in their essence or potentially. 
Whether they are not regarded as such, they should! Or at least should become arts as soon as possible. For the good of who's performing and the good of people surrounding. When thinking of any verb, let's add first “the art of ...” (the art of cleaning, the art of walking, the art of listening, the art of working, etc).

This way any kind of activity becomes a conscious Art if the doer is willing to. <b>Art is just an Attitude when doing any kind of task</b>. 
The aim is to highlight the artistic attitude and therefore imagine an unconventional space and a sort of enhancement, intensification and broadening of everyday experience. 
A predisposition that takes place during the gerund tense of the verb. The Present happens continuously, therefore, is the gerund the most relevant verb-form.

Art is nor the result of a set of trained skills applied to a task, neither is a theoretical concept.
Art is the ability of any culture or age human being to <b>sustain an attitude of Enjoying</b> any task, doing the best she/he knows during the process,
<b>trying to Improve</b> both personal skills and also the outcome: The artwork. The result or artwork should bring an intrinsic sense of satisfaction and happiness for both the doer and the others.

When the attitude is not linked to enjoying the process or unrelated to the search for improvement, the verb or art can be considered disconnected or empty. 
If it lacks love the outcome will not be nutritive for humankind. Even worse, if the attitude is from a non-acceptance position, the feelings involved can be resignation or resentment (when is a passive non-acceptance) 
or emotions like rebellion or resistance (when is an active non-acceptance) can be involved in the artwork. 

The attitude of Enjoying and Improving is nutritive when doer's Conscience (right brain hemisphere, the Unifier) is involved. Seeking for a common good it gives and spread satisfaction and happiness for everyone.
When who enjoys is the doer's Ego (left brain hemisphere, the Separator) the attitude is a mere eagerness to achieve personal success, not nutritive for others. The ego alone can't achieve genuine satisfaction or happiness since tends to separate, judge and analyze. 
More about mind [here]( https://github.com/IntegralDevs/integral-developing/blob/master/brains.md )

The condition of 'search for Improvement' is what becomes the training or skill upgrade of the human doer and it is also what opens the door to <b>originality or creativity</b>, to Change something in the process trying to improve the results or the task for the common good. 
The introduction of creativity and changes helps the condition of enjoying any given task.

### Kinds of Art: ###
    
All verbs in a language (what we refer as arts) are commonly used to describe Actions (such as jobs or skills) or Relations (between entities, objects, etc):

Any Relation should also be considered as an Art; it's only a matter of attitude and predisposition of the involved parties. Two main conditions are required: to be Enjoying and in Seek for Improvement.

The Actions or skills can be grouped in two verbs branches: 'Doing' (giving, making, communicating, etc) and 'Not doing' (receiving, attending, resting, etc) related respectively to the outward (outgoing energy) and inward (incoming energy) flows. 

The “good” or “bad” valuations are often subjective and relative to every situation and point of view and cannot be reduced easily as common. 
They are ego-centric sensations as a child, developing into etno-centric values (from cultural constructs inherited from society, the ones pressed-in by the economic interests, to the values of your affinity group, team, etc), and later the good-bad concepts develop hopefully into mundi-centric Values (what is good for all humanity). 

We might try to consider 'good or bad' as related to 'nutritive or not', but this is not enough. What matters is "nutritive for who?": the selfish ego, the affinity group or the whole human kind? More about ethics [here](https://gitlab.com/integraldevs/integral-developing/edit/master/ethics.md)

An example (automatic-natural, non-rational) is how children do art, plants do art, stones do art, water do art, atoms do art, planets do art, etc.
All Nature is performing Nutritive Arts. Though some disconnected humans (egos), mainly dominators, manipulators, and some so-called artists, omit this positive intentions, eager to gain control. But even if they seem to "enjoy" and “improve” the art of controlling, they never achieve or give satisfaction.

As seen above, we can suggest adjectives to define arts;
Nutritive or Full arts are positive for both nature and human beings while Non-nutritive or Empty arts are nor positive for humans neither for Nature. 
We suggest a relation between Nutritive/Non Nutritive and **the amount and quality of Love being felt by the doer during the creation process**.

Also, opposite (to Love) feelings are implied, as we postulate the existence of Toxic arts (charged with fear, hate and similar hostile feelings).

We also define Conscious arts or Automatic arts (both full or empty), related to the consciousness of the doer agent when performing a verb and Conscious refers only to be pay Attention (or not) to it. 
We reckon “unconscious” is not a real condition, just a mere lack of attention. <b>If you put enough attention everything can become conscious right now</b>.

The level of skills involved can also shape adjectives such as Skilled or Non-skilled arts (or Talented), and many in between (from newbie/initiated, etc, to professional/master). Beware that the skills concept can be dangerous when only related to the conceptual/theoretical part, while it's much more trusted any skill level related to practical experience (fruit of a factual 'doing' or direct 'experiencing'). 


Some examples:
- 'The art of creating' or 'the art of destroying' can be either nutritive (full) or not (empty) by nature, they can be conscious or not, they can be skilled or not, etc.
- 'The art of cleaning', can be empty if it'is not enjoyed or it's not seeking for the best results (the artwork). (a hint: to reach this attitude, you can imagine the non-existent 'clean' future result and start looking-for the imagined shape like a sculptor searches the desired shape in a raw block).
- 'The art of cooking', can be conscious or not, and can be full of love or empty. It can also hold dark emotions and produce toxic food, but it can be also very nutritive, way beyond the chemical or physical content (with love feeds much more).
- 'The art of killing', even if enjoyed and improved (there are weird people), can be very 'bad' if the killing is related to humans, but it can be ok when killing mosquitoes in a summer night or getting rid of plague at home, etc.
- 'The art of resting', is a non-active art (from the 'not doing' branch) but you can simply rest or you can instead play the art of resting (recovering more energy better).
- 'The art of giving', is nutritive to nature only when it produces joy for the giver. 

(more about data organization trees [here](https://github.com/IntegralDevs/integral-developing/blob/master/integral-data.md)

## What is Artwork? ##

If all verbs are considered Arts, then all creations are considered Artworks, this way everything done by someone (even if unknown or not human) is an artwork, and we are all artists (at least for some tasks). 
These artworks can be either nutritive, empty or toxic, depending on the attitude of the art Doer. 

Also, the attitude of each Receiver will define their singular experience of the artwork, this way, as a receiver, 
we can experience an artwork even if the Doer doesn't see it as such; that will make artworks production a constant we all participate. 

In addition, according to the artist attitude, the artworks will inherit the love being used, as well as the quality of materials, the level of the skills used, etc.

In the old conception of art and artwork exclusively done by “artists” as a subgroup, some “artists” are Communicators of ideas, 
thoughts or emotions through their artworks.

In recent centuries many of this communicators are signing their artworks, to spread their name (like a blogger publishing posts) to try to reach as many receivers as possible. Sometimes people give more importance to the author name than to the artwork content itself (another nonsense).

While other “artists” are anonymous, doing artworks and communicating to an audience, but they're not interested in signing or promoting any personal identity and their ego is not involved. We find examples of this practice in ancient arts, philosophies, spiritual graphics, tribal arts, zapatist arts, anonymous graffiti, etc.

In the old conception of art, “non-artists” are considered the Makers or Doers (aka the ones in charge to provide human living and development basic needs) together with anyone not falling in the "artist" category (an elite). 
Were considered “non-artists” also the art Critics, whose ego confusions defines whether a piece becomes or not "an artwork". Normal human things were not considered artworks.

In a new conception of Art and Artwork we propose, everybody is an artist and everything can be seen as an artwork. 

Every art has skilled masters able to teach about techniques, materials, tools, etc. 

Everyone can do healthy artworks as long as they want. 

Everyone can Communicate ideas or feelings as they wish using any art to produce any artwork, but these ideas can be either nutritive or not-nutritive for the receivers, 
as well as they can be understood or not. 

Everybody would be in need to filter people's messages inserted into artworks, prioritizing the more nutritive if a healthy life is desired. 

Everybody would need to filter the artwork itself in terms of Quality or using ethical values accordingly their development stage. 
We suggest, for a healthy attitude, “quality” as first, representing values, and “quantity” as secondary, representing success, as explained [here]( https://github.com/IntegralDevs/integral-developing/blob/master/ethics.md).

The challenge of structuring Artwork Types from an integral perspective is explained [here](https://github.com/IntegralDevs/integral-developing/blob/master/integral-data.md)


## Who is an Artist? ##

We reckon everyone as being an artist (specially non-human beings), though some of the humans do not identify themselves as artists, neither they realise the healing internal force art can give.
Perhaps to keep a separation between artists and non-artists was someway helpful (for the elite) to get humans more separated, in fact,  
the so-called 'Fine Arts' became accessible to high-society members only. The bond between arts and wealthy is complete nonsense.

We state that art can be found in every job; to become an 'artist' is sometimes a simple question of help out admired professionals or skilled artists, starting an apprenticeship sometimes even paid. 

In a scenery in which knowledge tends to be transformed into a market (health, research, etc.) the risk of manipulated and filtered set of information given for the mere purpose of selling a commodity is always there.

In a healthy world, the information should be free and accessible for everybody, and the knowledge and practical skills gained "learning by doing" accordingly to community needs. 

Some artists (according to our meaning of this term) are someway lost in their paths and can't achieve their full artistic attitude, 
so they perform empty arts, without love, with the result of creating empty artefacts, or even worse, creating some nefarious ones.

To consciously reconnect with its internal artist everyone is required to pay a lot of Attention. 
With paying attention and listening external or internal inputs we automatically stop the inner monologue, which is what we call what is produce by the left hemisphere. 
To stop this inner talk and leave some room for non-verbal mind (silent or visual, such as musical, etc) that we refer as right hemisphere (Conscience). 
The conscience mind, in searches of unity, is required to make healthy and *nutritive* artworks, enjoying the process and spreading out joy as results.


(v0.1 of this article is published at http://sebastiafreixa.com/en/the-arts-of-living and now also at [steemit](https://steemit.com/life/@sebastia-freixa/the-arts-of-living-developing-a-constructive-attitude-for-a-better-world))