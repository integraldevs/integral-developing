# integral-developing
<i>Building a constructive attitude, and tools for a better world</i>

&nbsp;

this repository is created to collectively develop an integral and healthy approach to Living as a whole, distilling a set of basic principles [Principles](/principles.md) to improve human relations, one of the main causes of the failure of many projects, redefining verbs as [Arts](/.arts.md) as a first step and then proceeding deeper into [Ethics](/ethics.md) and other issues such as [Brains](/brains.md) and how to organize data types from an integral perspective and how to distribute a p2p  [Common Database](/commonDBnet.md) and help the growth of a common integral forest of shared data-types trees and modular tools. 

Besides, here is a place where we may attempt to bring together, and join forces for the Common Good, all the "Integral" efforts made in a variety of format and approaches to address issues such as the so-called 'Integral Revolution', the so-called 'Integral Theory', the same concept of 'Commons', etc.  
In an attempt to focus on a realistic world-centric and human-centric angle, post ego-centric, post ethno-centric, post-capitalist and post-statist, with an eye to Nature and how it works, in an effort to learn and hopefully imitate how it works in our daily lives.

